package com.floow.codingchallenge;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

//This class reads input file and returns String Stream.
public class FileReaderService {
    private String fileName;
    private Stream<String> fileContent = null;

    public FileReaderService(String fileName) {
        this.fileName = fileName;
    }

    public Stream<String> readFile() throws IOException {
        BufferedReader bfr = new BufferedReader(new FileReader(fileName));
        Stream<String> resultLines = bfr.lines();
        return resultLines;
    }
}


