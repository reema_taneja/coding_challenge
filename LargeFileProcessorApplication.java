package com.floow.codingchallenge;

import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class LargeFileProcessorApplication {

    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();
        // Provide absolute path of the file as arg[1] in format - C:\\foldername\\file1.xml
        FileReaderService fileReader = new FileReaderService( args[1]);
        Stream<String> result = fileReader.readFile();
        FileProcessorService fileProcessorService = new FileProcessorService(result);
        ConcurrentHashMap conHashMap = fileProcessorService.processFile();
        //provide server name in args[3] in following format - mongodb://localhost:27017
        DBRepositoryService mongoRepo = new DBRepositoryService( args[3], conHashMap);
        mongoRepo.batchSave();
        long endTime = System.currentTimeMillis();
        long processTime = endTime - startTime;
        System.out.println("Total time to read, process and save file: " + processTime);
        System.out.println(mongoRepo.findMaxWordCount());
        System.out.println(mongoRepo.findMinWordCount());
    }
}






