package com.floow.codingchallenge;

import com.mongodb.client.*;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class DBRepositoryService {
    private String hostName = "";
    private ConcurrentHashMap<String, Integer> wordCounts = null;
    private MongoCollection<Document> collection= null;
    private static final String collectionName = "myCollection";
    private static final String dbName = "myMongoDB";

    public DBRepositoryService(String hostName, ConcurrentHashMap<String, Integer> hashMap) {
        this.hostName = hostName;
        this.wordCounts = hashMap;
        this.collection=getMongoCollection(dbName, collectionName);

    }

    private MongoCollection<Document> getMongoCollection(String dbName, String collectionName) {
        MongoClient mongoClient = MongoClients.create(hostName);
        MongoDatabase db = mongoClient.getDatabase(dbName);
        MongoCollection<Document> collection = db.getCollection(collectionName);
        return collection;
    }

    public void batchSave() {

        collection.createIndex(new Document("count", 1));
        List<Document> documents = new ArrayList<>();
        wordCounts.forEach((k, v) -> {
            documents.add(new Document("word", k).append("count", v));
            if (documents.size() == 100000) {
                collection.insertMany(documents);
                documents.clear();
            }
        });
        collection.insertMany(documents);

    }

    public String findMaxWordCount() {
        FindIterable<Document> docSorted1 = collection.find().sort(Sorts.descending("count")).limit(1);
        List<Document> docs1 = new ArrayList<>();
        docSorted1.into(docs1);
        String max_word = docs1.get(0).getString("word");
        Integer max_count = docs1.get(0).getInteger("count");
        return "Max Word: " + max_word + " " + "Count: " + max_count;
    }

    public String findMinWordCount() {
        FindIterable<Document> docSorted1 = collection.find().sort(Sorts.ascending("count")).limit(1);
        List<Document> docs1 = new ArrayList<>();
        docSorted1.into(docs1);
        String min_word = docs1.get(0).getString("word");
        Integer min_count = docs1.get(0).getInteger("count");
        return "Min Word: " + min_word + " " + "Count: " + min_count;
    }

}
