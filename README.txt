---

## How to run the program

1. download jar file and run java -jar CodingChallenge.jar -source <absolute file path with file name> -mongo mongodb://<hostname>:27017
2. Program requires jdk 1.8
3. Program will print the max word and its count as well min Word and its count on command window. it also prints total word count as hasmap size.

## Description of Application architecture
1. There are 4 java files - LargeFileProcessorApplication.java FileReaderService.java FileProcessorService.java DBRepositoryService.java.
2. LargeFileProcessorApplication contains the main method and invokes all other service classes to
		a) Read the file. It uses BufferedReader for faster file reading (FileReaderService.java)
		b) Process the file - identify unique words and their count. It uses ParallelStream and ConcurrentHashMap for concurrency. It stores the results in HashMap(FileProcessorService.java)
		c) Saves the words and their count in MongoDB. It uses insertMany() method of MongoDriver Class to minimize network I/O. In addition it uses batch save with batch size set to 100,000 documents (This is configurable) to avoid out of memory error.