package com.floow.codingchallenge;


import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class FileProcessorService {
    private Stream<String> result;
    private ConcurrentHashMap<String, Integer> wordCounts = new ConcurrentHashMap<>();

    public FileProcessorService(Stream<String> fileContent) {
        this.result = fileContent;
    }


    public ConcurrentHashMap<String, Integer> processFile() {
        long startTime1 = System.currentTimeMillis();

        result.parallel().forEach(line -> {
            Arrays.stream(line.replace("[^a-zA-Z_0-9]", "").
                    split("\\s+")).forEach(word -> {
                if (!wordCounts.containsKey(word)) {
                    wordCounts.put(word, 1);
                } else {
                    wordCounts.put(word, (wordCounts.get(word)) + 1);

                }
            });
        });
        System.out.println("HashMap Size: " + wordCounts.size());
        long endTime1 = System.currentTimeMillis();
        long processTime1 = endTime1 - startTime1;
        System.out.println("FileProcessorService -File processing Time: " + processTime1);
        return wordCounts;
    }

}
